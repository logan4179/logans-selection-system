using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace LoganPackages
{
	public class LSS_SelectableObject : MonoBehaviour
	{
		public int MyInteractionLayer = 0;
		public List<int> OpposingInteractionLayers;
		public LSS_SelectableObject MyOpposingObject;
		public bool SelectOpposing { get; set; }

		public UnityAction UAction_HighlightMe;
		public UnityAction UAction_UnHighlightMe;


		public UnityAction UAction_SelectedMeAsOpposing;
		public UnityAction UAction_SelectedMyOpposing;


		void Start()
		{

		}

		private void OnMouseEnter()
		{
			LSS_Manager.Instance.HandleCursorEnterOnInteractable(this);
		}

		/*private void OnMouseDown()
		{
			GameManager.Instance.LogansInteractionSystemManager.HandleMouseDownOnInteractable(this);

		}*/

		private void OnMouseUp()
		{
			LSS_Manager.Instance.HandleSelectionAttemptOnInteractable(this);

		}

		private void OnMouseExit()
		{
			LSS_Manager.Instance.HandleCursorExittOnInteractable(this);

		}
	}
}